# The Raku Conference 2021

The Raku Conference 2021 is the first ever conference entirely dedicated to the 🦋 Raku programming language.

## Author

Please contact me via [LinkedIn](https://www.linkedin.com/in/knarkhov/) or [Twitter](https://twitter.com/CondemnedCell). Your feedback is welcome at [narkhov.pro](https://narkhov.pro/contact-information.html).
